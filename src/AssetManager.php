<?php

namespace Vicimus\Asset;

/**
 * Easily manage css and js files
 *
 * @author Jordan
 */
class AssetManager
{
    /**
     * Holds all the CSS files added
     *
     * @var string[]
     */
    protected $styles = [];

    /**
     * Holds all the JS files added
     *
     * @var string[]
     */
    protected $scripts = [];

    /**
     * Read only protected property access
     *
     * @param string $property The property to read
     *
     * @return mixed
     */
    public function __get($property)
    {
        return $this->$property;
    }

    /**
     * Add a resource to the asset manager
     *
     * @param string $slug The slug to represent the file to avoid duplicates
     * @param string $file The path to the file
     *
     * @return $this
     */
    public function add($slug, $file)
    {
        $ext = $this->ext($file);
        if ($ext === '.css') {
            $this->styles[$slug] = $file;
            return $this;
        }

        if ($ext === '.js') {
            $this->scripts[$slug] = $file;
            return $this;
        }
        
        throw new \InvalidArgumentException(
            'Invalid extension used ['.$ext.']'
        );
    }

    /**
     * Grab the extension
     *
     * @param string $file The filename to inspect
     *
     * @return string
     */
    protected function ext($file)
    {
        return substr($file, strrpos($file, '.'));
    }

    /**
     * Get the styles output
     *
     * @return void
     */
    public function styles()
    {
        return $this->view('styles', $this->styles);
    }

    /**
     * Get the scripts output
     *
     * @return void
     */
    public function scripts()
    {
        return $this->view('scripts', $this->scripts);
    }

    /**
     * Generate a view
     *
     * @param string $view    The view to render
     * @param array  $payload The variables to pass to the view
     * @return string
     */
    protected function view($view, array $payload)
    {
        ob_start();
        require __DIR__.'/views/'.$view.'.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}
