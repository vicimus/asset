<?php

namespace Vicimus\Asset\Tests;

use Vicimus\Asset\AssetManager;

/**
 * Test the various methods of the AssetManager
 *
 * @author Jordan
 */
class AssetManagerTest extends TestCase
{
    /**
     * Hold the manager that will be used to test
     *
     * @var AssetManager
     */
    protected $asset;

    /**
     * Set the test up
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->asset = new AssetManager;
    }

    /**
     * Make sure the class got instantiated properly
     *
     * @return void
     */
    public function testInstantiation()
    {
        $this->assertInstanceOf(AssetManager::class, $this->asset);
    }

    /**
     * Test adding a css file
     *
     * @return void
     */
    public function testAddCSS()
    {
        $this->asset->add('my-css', 'file.css');
        $this->assertEquals(1, count($this->asset->styles));
        $this->assertEquals(0, count($this->asset->scripts));
    }

    /**
     * Test adding a css file
     *
     * @return void
     */
    public function testAddJS()
    {
        $this->asset->add('my-js', 'file.js');
        $this->assertEquals(0, count($this->asset->styles));
        $this->assertEquals(1, count($this->asset->scripts));
    }

    /**
     * Test outputting the styles
     *
     * @return void
     */
    public function testStylesOutput()
    {
        $this->asset->add('my-css', 'file.css');
        $output = $this->asset->styles();
        $this->assertContains('<link href="file.css"', $output);
    }

    /**
     * Test outputting the scripts
     *
     * @return void
     */
    public function testScriptsOutput()
    {
        $this->asset->add('my-js', 'file.js');
        $output = $this->asset->scripts();
        $this->assertContains('<script src="file.js"', $output);
    }

    /**
     * Add a file that shouldnt be added
     *
     * @expectedException \InvalidArgumentException
     * @return void
     */
    public function testInvalidFile()
    {
        $this->asset->add('my-invalid', 'file.doc');
    }
}
